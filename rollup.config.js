import babel from 'rollup-plugin-babel';
import eslint from 'rollup-plugin-eslint';
import replace from 'rollup-plugin-replace';
import postcss from 'rollup-plugin-postcss';
import svelte from 'rollup-plugin-svelte';

import simplevars from 'postcss-simple-vars';
import nested from 'postcss-nested';
import cssnext from 'postcss-cssnext';
import cssnano from 'cssnano';

export default {
    entry: 'app/main.js',
    dest: 'dist/bundle.js',
    format: 'iife',
    plugins: [
        babel({
            include: 'app/*.js'
        }),
        eslint({
            include: 'app/*.js'
        }),
        postcss({
            plugins: [
                simplevars(),
                nested(),
                cssnext({ warnForDuplicates: false}),
                cssnano()
            ],
            extensions: ['.css']
        }),
        replace({
            exclude: 'node_modules/**',
            ENV: JSON.stringify(process.env.NODE_ENV || 'dev')
        }),
        svelte({
            extensions: ['.html'],
            include: 'app/**.html'
        })
    ]
};