import MyBffBl from './mybffbl.html';
import './css/mybffbl.css';
import bucket_list_suggestions from './bucket_list_suggestions.json';
import bucket_list_themes from './bucket_list_themes.json';

new MyBffBl({
    target: document.querySelector('#bffbl-main-content'),
    data: {
        bucket_list_suggestions: bucket_list_suggestions,
        bucket_list_themes: bucket_list_themes
    }
});

if (ENV !== 'prod') {

    document.write(
        '<script src="http://' + (location.host || 'localhost').split(':')[0] +
        ':35729/livereload.js?snipver=1"></' + 'script>'
    );
}